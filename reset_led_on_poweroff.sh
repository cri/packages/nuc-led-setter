#!/bin/sh

led_path="/proc/acpi/nuc_led"
if [ -f "$led_path" ]; then
    echo 'ring,100,none,off' > "$led_path"
else
    >&2 echo "NUC led is not available"
fi

modprobe -r nuc_led
