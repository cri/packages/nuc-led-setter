#!/usr/bin/env python3
"""
Module retrieving the led status from org.cri.MachineState on DBus and
applying a color in relation with the status.
"""
import logging
import math
import os
import sys
import threading
import time
from gi.repository import GLib
from pydbus import SystemBus

# Logging
LOGGER = logging.getLogger('__name__')
logging.basicConfig(level=logging.DEBUG, format='%(levelname)s: %(message)s')

HANDLER_DEBUG = logging.StreamHandler(sys.stdout)
HANDLER_DEBUG.setLevel(logging.DEBUG)

HANDLER_INFO = logging.StreamHandler(sys.stdout)
HANDLER_INFO.setLevel(logging.INFO)

HANDLER_ERROR = logging.StreamHandler(sys.stderr)
HANDLER_ERROR.setLevel(logging.ERROR)

LOGGER.addHandler(HANDLER_DEBUG)
LOGGER.addHandler(HANDLER_INFO)
LOGGER.addHandler(HANDLER_ERROR)

# Const Values
SPLITTING_CHAR_COLOR = ';' # You can edit this but do not use '_'
ERROR_TIMEOUT = 5

COLOR_MODE_MIN_DELAY = 0.1
COLOR_MODE_MAX_DELAY = 5.0
COLOR_MODE_MIN_DURATION = 1.0
COLOR_MODE_MAX_DURATION = 86400.0
COLOR_MODE_MIN_SAMPLING_RATE = 0.04

# pylint: disable=attribute-defined-outside-init
class LedColor():
    """Class used to create a color for the led"""
    # pylint: disable=too-many-instance-attributes
    def __init__(self, color, brightness=100, mode="none", led_type="ring"):
        self.led_type = led_type
        self.brightness = brightness
        self.mode = mode
        self.color = color

    @property
    def led_type(self):
        """Gets self._led_type"""
        return self._led_type

    @led_type.setter
    def led_type(self, led_type):
        """Sets self._led_type"""
        if led_type not in LedColor.AVAILABLE_LED_TYPE:
            raise ValueError(f"Invalid led_type: {led_type}")
        self._led_type = led_type

    @property
    def brightness(self):
        """Gets self._brightness"""
        return self._brightness

    @brightness.setter
    def brightness(self, brightness):
        """Sets self._brightness"""
        if brightness < 0 or brightness > 100:
            raise ValueError(f"Invalid brightness: {brightness}, \
must be between 0 and 100 included")
        self._brightness = brightness

    @property
    def mode(self):
        """Gets self._mode"""
        return self._mode

    @mode.setter
    def mode(self, mode):
        """Sets self._mode"""
        if mode not in LedColor.AVAILABLE_MODES:
            raise ValueError(f"Invalid mode: {mode}")
        self._mode = mode

    @property
    def color(self):
        """Gets self._color"""
        return self._color

    @color.setter
    def color(self, color):
        """Sets self._color"""
        available_colors = LedColor.AVAILABLE_COLORS_RING
        if self.led_type == "power":
            available_colors = LedColor.AVAILABLE_COLORS_BUTTON
        if color not in available_colors:
            raise ValueError(f"Invalid color: {color}")
        self._color = color

    def __str__(self):
        return f"{self.led_type},{self.brightness},{self.mode},{self.color}"

    AVAILABLE_LED_TYPE = [
        "power",
        "ring",
    ]

    AVAILABLE_COLORS_RING = [
        "off",
        "blue",
        "cyan",
        "green",
        "pink",
        "red",
        "white",
        "yellow",
    ]

    AVAILABLE_COLORS_BUTTON = [
        "amber",
        "blue",
        "off",
    ]

    AVAILABLE_MODES = [
        "none",
        "blink_fast",
        "blink_medium",
        "blue_slow",
        "fade_fast",
        "fade_medium",
        "fade_slow",
    ]

DEFAULT_COLORS = {color: LedColor(color) for color in LedColor.AVAILABLE_COLORS_RING}

def colors_from_list(colors):
    """Transform a list of color into a list of LedColor using the DEFAULT_COLORS"""
    return [DEFAULT_COLORS[color] for color in colors]

# pylint: disable=attribute-defined-outside-init
class LedColorMode():
    """Class used to create a special color for the led"""

    def __init__(self, mode_name, duration=20, delay=1):
        self.color_array = mode_name
        self.duration = duration
        self.delay = delay

    @property
    def color_array(self):
        """Gets self._color_array"""
        return self._color_array

    @color_array.setter
    def color_array(self, mode_name):
        """Sets self._color_array"""
        if mode_name not in LedColorMode.colors_modes:
            raise ValueError(f"Invalid mode name: {mode_name}")
        self._color_array = LedColorMode.colors_modes[mode_name]

    @property
    def duration(self):
        """Gets self._duration"""
        return self._duration

    @duration.setter
    def duration(self, duration):
        """Sets self._duration"""
        if duration < COLOR_MODE_MIN_DURATION or duration > COLOR_MODE_MAX_DURATION:
            raise ValueError(f"Invalid duration: {duration}, \
must be between {COLOR_MODE_MIN_DURATION} and \
{COLOR_MODE_MAX_DURATION} included")
        self._duration = duration

    @property
    def delay(self):
        """Gets self._delay"""
        return self._delay

    @delay.setter
    def delay(self, delay):
        """Sets self._delay"""
        if delay < COLOR_MODE_MIN_DELAY or delay > COLOR_MODE_MAX_DELAY:
            raise ValueError(f"Invalid delay: {delay}, \
must be between {COLOR_MODE_MIN_DELAY} and \
{COLOR_MODE_MAX_DELAY} included")
        self._delay = delay

    colors_modes = {
        "rainbow": colors_from_list(["red", "yellow", "green", "cyan", "blue", "pink", "white"]),
        "pinpon": colors_from_list(["red", "white"]),
        "police": colors_from_list(["red", "blue"]),
        "police-nationale": colors_from_list(["blue", "white"]),
        "guirlande": colors_from_list(["green", "red"]),
        }

class NucLed():
    """
    Class used to access the led file
    """
    def __init__(self, sys_bus, led_path):
        self._sys_bus = sys_bus
        self._led_path = led_path
        self._current_led = LedColor("blue")
        self._error_color = LedColor("white", mode="blink_fast")
        self._error_mode = False
        self._thread_setter = None
        self._keep_thread_alive = True

    @property
    def current_led(self):
        """Gets self._current_led"""
        return self._current_led

    # Signal handler
    def set_nuc_led_callback(self, _sender, _obj, _iface, _signal, params):
        """Callback function when a signal is fired"""
        if "LedColor" in params[1]:
            new_status = params[1]["LedColor"]
            self.apply_color(new_status)

    def _reset_override_caller(self):
        """Reset the override attribute if there is an error"""
        logging.info("Resetting color after the end of a special mode")
        try:
            machine_state_obj = self._sys_bus.get("org.cri.MachineState", "/org/cri/MachineState")
            machine_state_interface = machine_state_obj["org.cri.MachineState"]
            machine_state_interface.LedColorOverride("reset")
        except GLib.Error:
            logging.error("Failed to reset the override mode")

    def force_color_signal(self):
        """Force the object maintaining the state to resend a new color signal"""
        logging.info("Requesting a signal from the object maintaining the state")
        try:
            machine_state_obj = self._sys_bus.get("org.cri.MachineState", "/org/cri/MachineState")
            machine_state_interface = machine_state_obj["org.cri.MachineState"]
            machine_state_interface.ForceColorSignal()
        except GLib.Error:
            logging.error("Failed to ask for new color signal")

    def _reset_error_mode(self):
        """Reset the led after an error"""
        logging.info("Exitting error mode")
        self._error_mode = False
        self._reset_override_caller()
        self.set_nuc_led(self._current_led)

    def _notify_error(self):
        """Notify that the user has made an error"""
        logging.info("Notifying error")
        self.set_nuc_led(self._error_color)
        self._error_mode = True
        GLib.timeout_add_seconds(ERROR_TIMEOUT, self._reset_error_mode)

    def _handle_reset(self, color):
        """Handles the empty color, resetting"""
        if color[0] == "":
            self.kill_thread()
            return True
        return False

    def _handle_default_colors(self, color):
        """Handle default value for colors"""
        if len(color) != 1:
            return False
        try:
            self._current_led = DEFAULT_COLORS[color[0]]
        except KeyError:
            logging.info("Invalid default color")
            return False
        return True

    def _handle_color_modes(self, color):
        """Handles the keyword for the special modes"""
        try:
            mode = LedColorMode(color[0], *[float(elt) for elt in color[1:]])
        except (ValueError, TypeError) as error:
            logging.info(error)
            return False
        logging.info("Applying mode: %s", color[0])
        self.kill_thread()
        self._thread_setter = threading.Thread(target=self.thread_setter_func,
                                               args=(mode.color_array,
                                                     mode.duration,
                                                     mode.delay),
                                               daemon=True)
        self._thread_setter.start()
        return True

    def _verify_color(self, color):
        """Checks if the color is legit"""
        # If it's not special, we check for the validity
        if len(color) != 3:
            self._notify_error()
            return False
        try:
            self._current_led = LedColor(color[0], int(color[1]), color[2])
        except (ValueError, KeyError) as error:
            logging.info(error)
            self._notify_error()
            return False
        return True

    color_functions = [
        _handle_reset,
        _handle_default_colors,
        _handle_color_modes,
        _verify_color
        ]

    def apply_color(self, color):
        """Verify the status and call the led setter"""
        color_splitted = [elt.lower() for elt in color.split(SPLITTING_CHAR_COLOR)]
        logging.info("New incoming color from signal: %s", color_splitted)
        for func in self.color_functions:
            if func(self, color_splitted):
                break
        if not self._thread_setter or not self._thread_setter.is_alive():
            if not self._error_mode:
                logging.info("Applying the color: %s", str(self._current_led))
                self.set_nuc_led(self._current_led)

    def set_nuc_led(self, led_color):
        """Set the led color"""
        try:
            led_file = open(self._led_path, "w")
        except IOError as error:
            logging.error("Unable to acces the led driver, %s", error)
        else:
            with led_file as led:
                led.write(str(led_color))

    # Thread
    def kill_thread(self):
        """Kill the thread if needed"""
        if self._thread_setter and self._thread_setter.is_alive():
            self._keep_thread_alive = False
            self._thread_setter.join()

    @staticmethod
    def get_color_for_animation(origin_color, elapsed_time, delay):
        """Return a color with a modified brightness"""
        ratio = math.fabs(math.sin(elapsed_time / delay * math.pi))
        return LedColor(origin_color.color,
                        math.trunc(origin_color.brightness * ratio),
                        origin_color.mode)


    def thread_setter_func(self, color_array, duration=20, delay=1):
        """Function used to apply special color status"""
        logging.debug("Starting the thread for the special led status")
        try:
            led_file = open(self._led_path, "wb", 0)
        except IOError as error:
            logging.error("Unable to acces the led driver, %s", error)
        else:
            with led_file as led:
                i = 0
                start_time = time.time()
                elapsed_time = 0
                total_time = 0
                sampling_delay = max(delay / 100, COLOR_MODE_MIN_SAMPLING_RATE)
                logging.debug("Applying pattern with sampling: %.2f", sampling_delay)

                while self._keep_thread_alive and total_time <= duration:
                    color = NucLed.get_color_for_animation(color_array[i], elapsed_time, delay)
                    try:
                        led.write(str(color).encode())
                    except IOError as error:
                        self._keep_thread_alive = False
                        logging.error("Unable to acces the led driver, %s", error)

                    time.sleep(sampling_delay)
                    total_time = time.time() - start_time
                    nb_occur, elapsed_time = divmod(total_time, delay)
                    i = int(nb_occur % len(color_array))

        if self._keep_thread_alive:
            self.set_nuc_led(self._current_led)
            self._reset_override_caller()
        self._keep_thread_alive = True
        logging.debug("Exiting the thread for the special led status")

def main():
    """Main function"""
    led_path = "/proc/acpi/nuc_led"
    if not os.path.exists(led_path):
        logging.error("Not a NUC, exiting...")
        exit(1)
    sys_bus = SystemBus()
    led = NucLed(sys_bus, led_path)
    sys_bus.subscribe("org.cri.MachineState", "org.freedesktop.DBus.Properties",
                      "PropertiesChanged", "/org/cri/MachineState", "org.cri.MachineState",
                      0, led.set_nuc_led_callback)
    led.force_color_signal()
    led.set_nuc_led(led.current_led)
    loop = GLib.MainLoop()
    loop.run()

if __name__ == "__main__":
    main()
