"""
Entrypoint for the nuc_led_setter module
"""

from . import nuc_led_setter

def cmd_nuc_led_setter():
    """
    Entrypoint for the nuc_led_setter module
    """
    nuc_led_setter.main()
