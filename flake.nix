{
  description = "Read the current lock status and set the NUC's led accordingly.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    futils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, futils } @ inputs:
    let
      inherit (nixpkgs) lib;
      inherit (lib) recursiveUpdate;
      inherit (futils.lib) eachDefaultSystem defaultSystems;

      nixpkgsFor = lib.genAttrs defaultSystems (system: import nixpkgs {
        inherit system;
        overlays = [ self.overlay ];
      });

      poetryArgs = pkgs: {
        projectDir = self;
        src = self;

        overrides = pkgs.poetry2nix.overrides.withDefaults (self: super: {
          black = null;
          pre-commit = null;
          virtualenv = null;
        });

        postInstall = ''
          install -Dm755 $src/reset_led_on_poweroff.sh $out/bin/reset_led_on_poweroff.sh
        '';

        meta = with lib; {
          inherit (self) description;
          maintainers = with maintainers; [ risson ];
        };
      };

      anySystemOutputs = {
        overlay = final: prev: {
          nuc-led-setter = final.poetry2nix.mkPoetryApplication (poetryArgs final);
        };
      };

      multipleSystemsOutputs = eachDefaultSystem (system:
        let
          pkgs = nixpkgsFor.${system};
        in
        {
          devShell = pkgs.mkShell {
            buildInputs = with pkgs; [
              git
              nixpkgs-fmt
              poetry
              (poetry2nix.mkPoetryEnv (removeAttrs (poetryArgs pkgs) [ "meta" "src" "postInstall" ]))
              pre-commit
            ];
          };

          packages = {
            inherit (pkgs) nuc-led-setter;
          };
          defaultPackage = self.packages.${system}.nuc-led-setter;

          apps = {
            nuc-led-setter = {
              type = "app";
              program = "${self.defaultPackage.${system}}/bin/nuc-led-setter";
            };
          };
          defaultApp = self.apps.${system}.nuc-led-setter;
        });
    in
    recursiveUpdate multipleSystemsOutputs anySystemOutputs;
}
